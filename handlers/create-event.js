const AWS = require('aws-sdk')
const docClient = new AWS.DynamoDB.DocumentClient()

function createEvent(request) {
    //console.log("createEvent data=",request)
    return docClient.put({
        TableName: 'events',
        Item: request
    }).promise()
    .then((res) => {
        console.log('event created')
        return res
    })
}

module.exports = createEvent