const AWS = require('aws-sdk')
const docClient = new AWS.DynamoDB.DocumentClient()

function createCalendar(request) {
    var defaultWork = [
        {
            Start: "13",
            End: "19"            
        },
        {
            Start: "13",
            End: "19"            
        },
        {
            Start: "13",
            End: "19"            
        },
        {
            Start: "13",
            End: "19"            
        },
        {
            Start: "13",
            End: "19"            
        }        
    ]
    //var work = defaultWork
    if (!request.Work)
        request.Work = defaultWork
    return docClient.put({
        TableName: 'calendars',
        Item: request
    }).promise()
    .then((res) => {
        console.log('calendar created')
        return res
    })
}

module.exports = createCalendar