const AWS = require('aws-sdk')
const docClient = new AWS.DynamoDB.DocumentClient()
const wait = require('wait-for-stuff')

const dayMillis = 86400000;
const length = 30;

function getCalendars(calId) {
    if (calId) {
        console.log("getCalendars calId =",calId.toString())
        var params = {
            TableName: 'calendars',
            Key: {
                "calId": calId.toString()
            }
        }

        return docClient.get(params).promise()
        .then(result => result.Item)
    }
    return docClient.scan({
        TableName: 'calendars'
    }).promise()
    .then(result => result.Items)
}

function initCalendar(request) {
    console.log("initCalendar ", request.calId);
    var id = request.calId;
    var calendar = wait.for.promise(getCalendars(id));
    try {
        console.log("Calendar = ", calendar);
        var count = 0;
        var items = {
            RequestItems: {
                events: []
            }
        };
        var now = new Date();

        var millis = Date.now();
        var day = now.getDay();
        for (count = 0; count < length; ) {
            for (i = day; i < 6; i++) {
                if (i == 0)
                    continue;
                var date = new Date(millis);
                var hours = Math.floor(calendar.Work[(i - 1)].Start);
                var minutes = Math.floor(calendar.Work[(i - 1)].Start - hours) * 60.0;
                var dauer = Math.floor((calendar.Work[(i - 1)].End - calendar.Work[(i - 1)].Start) * 60.0);
                date.setHours(hours);
                date.setMinutes(minutes);
                //console.log("Hours = ", hours);
                //console.log("Minutes = ", minutes);
                var item = {
                    //PutRequest: {
                        //Item: {
                            'calId': id,
                            'dateTime': date.toISOString(),
                            'busy': false,
                            'Dauer': dauer
                        //}
                    //}
                };
                //items.RequestItems.events.push(item);
                var ret = wait.for.promise(docClient.put({
                    TableName: 'events',
                    Item: item
                }).promise());
                millis += dayMillis;
                count++;
                setTimeout(() =>{},200);
            }
            console.log("number of events = ", items.RequestItems.events.length);
            day = 1;
            count += 2;
            millis += 2 * dayMillis;
        }

        //console.log("1. event = ", items.RequestItems.events[0]);
        //var len = items.RequestItems.events.length;
        //var params = {
        //    RequestItems: {
        //        'events': []
        //    }
        //}
        //params.RequestItems['events'] = items.RequestItems.events.slice(0,(len - 1));
        //console.log("1. event = ", params.RequestItems['events'][0]);

        //var res = wait.for.function(docClient.batchWrite,params);
        //if (res[0]) {
        //    console.log("Error executing batchWrite",res[0])
        //}
        //else {
        //    console.log("initCalendar finished");
        //}
    }
    catch (e) {
        console.log(e.message);
    }
}

module.exports = initCalendar;