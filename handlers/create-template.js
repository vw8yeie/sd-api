const AWS = require('aws-sdk')
const docClient = new AWS.DynamoDB.DocumentClient()

function createTemplate(request) {
    //console.log("createTemplate data=",request)
    return docClient.put({
        TableName: 'templates',
        Item: request
    }).promise()
    .then((res) => {
        console.log('template created')
        return res
    })
}

module.exports = createTemplate