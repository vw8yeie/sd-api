const AWS = require('aws-sdk')
const docClient = new AWS.DynamoDB.DocumentClient()

function getCalendars(calId) {
    if (calId) {
        console.log("getCalendars calId =",calId.toString())
        var params = {
            TableName: 'calendars',
            Key: {
                "calId": calId.toString()
            }
        }

        return docClient.get(params).promise()
        .then(result => result.Item)
    }
    return docClient.scan({
        TableName: 'calendars'
    }).promise()
    .then(result => result.Items)
}

module.exports = getCalendars