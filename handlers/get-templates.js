const AWS = require('aws-sdk')
const docClient = new AWS.DynamoDB.DocumentClient()

function getTemplates(tplId) {
    if (tplId) {
        console.log("getTemplates tplId =",tplId.toString())
        var params = {
            TableName: 'templates',
            Key: {
                "tplId": tplId.toString()
            }
        }

        return docClient.get(params).promise()
        .then(result => result.Item)
    }
    return docClient.scan({
        TableName: 'templates'
    }).promise()
    .then(result => result.Items)
}

module.exports = getTemplates