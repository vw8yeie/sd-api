'use strict'

const Api = require('claudia-api-builder')
const api = new Api()

const str2json = require('string-to-json')

const getCalendars = require('./handlers/get-calendars')
const getTemplates = require('./handlers/get-templates')
const createCalendar = require('./handlers/create-calendar')
const initCalendar = require('./handlers/init-calendar')
const createTemplate = require('./handlers/create-template')
const createEvent = require('./handlers/create-event')

api.get('/get-calendars', () => {
    return getCalendars()
})

api.get('/get-calendars/{id}', (request) => {
    return getCalendars(request.pathParams.id)
})

api.post('/create-calendar', (request) => {
    return createCalendar(request.body)
}, {
    success: 201,
    error: 400
})

api.post('/init-calendar', (request) => {
    return initCalendar(request.body)
}, {
    success: 201,
    error: 400    
})

api.post('/create-template', (request) => {
    return createTemplate(request.body)
}, {
    success: 201,
    error: 400
})

api.get('/get-templates', () => {
    return getTemplates()
})

api.get('/get-templates/{id}', (request) => {
    return getTemplates(request.pathParams.id)
})

api.post('/create-event', (request) => {
    return createEvent(request.body)
}, {
    success: 201,
    error: 400    
})

module.exports = api